# файл send.py

import pika

x=15
connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()


channel.queue_declare(queue='hello')

while x>0:
    channel.basic_publish(exchange='',
                      routing_key='hello',
                      body='Hello World! #' + str(x))
    print(" [x] Sent 'Hello World!' #" + str(x))
    x-=1
connection.close()