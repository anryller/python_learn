import math

class Rectangle:

    height = 0
    width = 0

    def get_area(self, height, width):
        c = height * width
        return c

    def get_per(self, height, width):
        p1 = height*2
        p2 = width*2
        return p1 + p2

a1 = Rectangle()

print(a1.get_area(2, 5))
print(a1.get_per(4, 2))

