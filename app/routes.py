# как добавить циклы (routes.py):
from collections import namedtuple

from flask import render_template, redirect, url_for, request
from app.models import Post
from app.forms import PostForm
import app.funtions


from . import app, db
Message = namedtuple('Message', 'text tag checker')
messages = []


@app.route('/', methods=['GET'])
@app.route('/index')
def index():
    user = {'username': 'Никита'}
    posts = Post.query.all()
    return render_template('index.html', title='Home', user=user, posts=posts)

@app.route('/create', methods=['GET', 'POST'])
def create():
    form = PostForm()
    if form.validate_on_submit():
        p = Post(title=form.title.data, text=form.text.data)
        db.session.add(p)
        db.session.commit()
        return redirect('/index')
    return render_template('create.html', title='Sign In', form=form)

@app.route('/main')
def main():
    return render_template('main.html', messages=messages)

@app.route('/add_message', methods=['POST'])
def add_message():
    text = request.form['text']
    tag = request.form['tag']
    checker = 'not checked'
    if request.form.get('match'):
        checker = 'checked'
    messages.append(Message(text, tag, checker))
    return redirect(url_for('main'))

@syncbase.route('/syncbase', methods=['GET', 'POST'])
def syncbase():
    base =