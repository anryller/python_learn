import math


def graph(x, y):
    for z in y:
        tab = '\t'
        result=int(x(z))
        if len(str(z)) < 3:
            tab='\t\t'
        print(str(z) + ":" + tab + '#'*result)

graph(math.sqrt, [9, 25, 100])

def mysquad(myside):
    mysquare = myside**2
    myper = myside*4
    mydia = math.sqrt(myside**2*2)
    return (mysquare, myper, mydia)

print(mysquad(6))
print(type(mysquad(6)))

def