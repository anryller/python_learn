from random import randint

print("Zadacha 1")
x_list=range(1,101)


for v in x_list:
    if v%15==0:
        print("FizzBuzz")
        continue
    elif v%3==0:
        print("Fizz")
        continue
    elif v%5==0:
        print("Buzz")
        continue
    print(v)

print()
print("ZADAChA 2")
l=[4, 7, 8, 0, 12, 14]
n=0
for x in l:
    n+=x
print('Сумма всех чисел списка равна ' + str(n))

print()
print("ZADAChA 3")
n=0
mysum=0

while n != len(l):
    count=l[n]
    n+=1
    mysum=mysum + count
print('Сумма всех чисел списка равна ' + str(mysum))
